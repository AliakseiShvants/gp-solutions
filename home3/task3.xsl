<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:key name="itemsKey" match="//item" use="substring(@Name, 1, 1)"/>
    
    <!-- solution with APPLY-TEMPLATES -->
    <xsl:template match="list">
        <list>
            <xsl:apply-templates 
                select="item[generate-id(.) = 
                generate-id(key('itemsKey', substring(@Name, 1, 1)))]">
                <xsl:sort select="@Name"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>
    
    <xsl:template match="item">
        <capital value="{substring(@Name, 1, 1)}">
            <xsl:for-each select="key('itemsKey', substring(@Name, 1, 1))">
                <xsl:sort select="@Name"/>
                <name>
                    <xsl:value-of select="@Name"/>
                </name>
            </xsl:for-each>
        </capital>  
    </xsl:template>
    <!-- end  -->
    
    <!-- solution with CALL-TEMPLATES -->
    <xsl:template match="list">
        <list>
            <xsl:call-template name="alphabetPointer"/>            
        </list>
    </xsl:template>
    
    <xsl:template name="alphabetPointer">
        <xsl:variable name="capitals" 
            select="item[generate-id(.) = 
            generate-id(key('itemsKey', substring(@Name, 1, 1)))]"/>
        
        <xsl:for-each select="$capitals">
            <xsl:sort select="@Name"/>
            <capital value="{substring(@Name, 1, 1)}">
                <xsl:for-each select="key('itemsKey', substring(@Name, 1, 1))">
                    <xsl:sort select="@Name"/>
                    <name>
                        <xsl:value-of select="@Name"/>
                    </name>
                </xsl:for-each>
            </capital> 
        </xsl:for-each>  
    </xsl:template>
</xsl:stylesheet>