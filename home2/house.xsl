<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    version="1.0"
    xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema">
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template  match="/">
        <AllRooms>
            <xsl:call-template name="AllRooms"/>
        </AllRooms>
    </xsl:template>
    
    <xsl:template name="AllRooms">
        <xsl:for-each select="//r:Room">
            <xsl:sort select="ancestor::h:House/@City"/>
            <xsl:sort select="ancestor::h:Block/@number"/>
            <xsl:sort select="./@number" data-type="number"/>
            <xsl:call-template name="Room"/>
        </xsl:for-each>              
    </xsl:template>
    
    <xsl:template name="Room">    
        <Room>
            <xsl:call-template name="Address"/>
            <xsl:call-template name="houseRoomsCount"/>
            <xsl:call-template name="blockRoomsCount"/>
            <xsl:call-template name="houseGuestsCount"/>
            <xsl:call-template name="guestsPerRoomAverage"/>
            <xsl:call-template name="Allocated"/>
        </Room>
    </xsl:template>
    
    <xsl:template name="Address">
        <xsl:variable name="city" select="ancestor::h:House/@City"/>
        <xsl:variable name="street" select="ancestor::h:House/i:Address/text()"/>
        <xsl:variable name="block" select="ancestor::h:Block/@number"/>
        <xsl:variable name="room" select="./@number"/>
        
        <xsl:variable name="LETTERS" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
        <xsl:variable name="letters" select="'abcdefghijklmnopqrstuvwxyz'"/>
        
        <xsl:variable name="streetNameLen" select="string-length(translate($street,
            translate($street, concat($letters, $LETTERS, ' '), ''), ''))"/>
        <xsl:variable name="address" select="concat(
            translate( $city, $letters, $LETTERS), '/', 
            substring($street, 1 ,$streetNameLen - 1), '/',
            substring($street, $streetNameLen + 1), '/',
            $block, '/', $room)"/>
        
        <Address>
            <xsl:value-of select="$address"/>
        </Address>
    </xsl:template>
    
    <xsl:template name="houseRoomsCount">
        <HouseRoomsCount>
            <xsl:value-of select="count(ancestor::h:House//r:Room)"/>
        </HouseRoomsCount>
    </xsl:template>
    
    <xsl:template name="blockRoomsCount">
        <BlockRoomsCount>
            <xsl:value-of select="count(ancestor::h:Block//r:Room)"/>
        </BlockRoomsCount>
    </xsl:template>
    
    <xsl:template name="houseGuestsCount">
        <HouseGuestsCount>
            <xsl:value-of select="sum(ancestor::h:House//@guests)"/>
        </HouseGuestsCount>
    </xsl:template>
    
    <xsl:template name="guestsPerRoomAverage"> 
        <xsl:variable name="guestsAmount" select="sum(ancestor::h:House//@guests)"/>
        <xsl:variable name="roomsAmount" select="count(ancestor::h:House//r:Room)"/>
        <GuestsPerRoomAverage>
            <xsl:value-of select="floor($guestsAmount div $roomsAmount)"/>
        </GuestsPerRoomAverage>
    </xsl:template>
    
    <xsl:template name="Allocated">
        <xsl:variable name="amount" select="./@guests"/>
        <Allocated Quarter="{$amount = 4}" Triple="{$amount = 3}" 
            Double="{$amount = 2}" Single="{$amount = 1}"/>          
    </xsl:template>
</xsl:stylesheet>