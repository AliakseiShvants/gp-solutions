<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        <text>
            <xsl:apply-templates/>
        </text>
    </xsl:template>
    
    <xsl:template match="Guest">
        <xsl:variable name="type" select=".//Type/text()"/>
        <xsl:variable name="age" select=".//@Age"/>
        <xsl:variable name="nation" select=".//@Nationalty"/>
        <xsl:variable name="gender" select=".//@Gender"/>
        <xsl:variable name="name" select=".//@Name"/>
        <xsl:variable name="address" select=".//Address/text()"/>
        <xsl:variable name="info" select="concat($type, '|', $age, '|', $nation, '|', $gender, '|', $name, '|', $address, '#')"/>
        
        <xsl:value-of select="$info"/>
    </xsl:template>
</xsl:stylesheet>