<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        <xsl:variable name="text" select="//text()"/>
        <Guests>
            <xsl:if test="not(string-length($text) = 0)">
                <xsl:call-template name="recParser">
                    <xsl:with-param name="text" select="$text"/>
                </xsl:call-template>
            </xsl:if>
        </Guests>
    </xsl:template>
    
    <!-- solution with recursion -->
    <xsl:template name="recParser">
        <xsl:param name="text"/>
        <xsl:call-template name="transform">
            <xsl:with-param name="token" select="substring-before($text, '#')"/>
        </xsl:call-template>
        
        <!-- recursive call -->
        <xsl:variable name="rest" select="substring-after($text, '#')"/>
        <xsl:if test="contains($rest, '#')">
            <xsl:call-template name="recParser">
                <xsl:with-param name="text" select="$rest"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="transform">
        <xsl:param name="token"/>
        <xsl:variable name="type" select="substring-before($token, '|')"/>
        
        <xsl:variable name="temp" select="substring-after($token, '|')"/>
        <xsl:variable name="age" select="substring-before($temp, '|')"/>
        
        <xsl:variable name="temp1" select="substring-after($temp, '|')"/>
        <xsl:variable name="nation" select="substring-before($temp1, '|')"/>
        
        <xsl:variable name="temp2" select="substring-after($temp1, '|')"/>
        <xsl:variable name="sex" select="substring-before($temp2, '|')"/>
        
        <xsl:variable name="temp3" select="substring-after($temp2, '|')"/>
        <xsl:variable name="name" select="substring-before($temp3, '|')"/>
        
        <xsl:variable name="temp4" select="substring-after($temp3, '|')"/>
        <xsl:variable name="address" select="translate($temp4, '|#', '')"/>
        <Guest Name="{$name}" Gender="{$sex}" Nationalty="{$nation}" Age="{$age}">
            <Type>
                <xsl:value-of select="$type"/>
            </Type>
            <Profile>
                <Address>
                    <xsl:value-of select="$address"/>
                </Address>
            </Profile>
        </Guest>
    </xsl:template>
</xsl:stylesheet>