package com.soapserver.core.dao.impl;

import com.soapserver.core.dao.CountriesDAO;
import com.soapserver.core.dao.HotelsDAO;
import com.soapserver.core.dao.JdbcExt;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class HotelsDAOImpl extends JdbcExt implements HotelsDAO {
	
	private static final String CITIES_NAMES = "SELECT DISTINCT NAME FROM GPT_LOCATION_NAME LN JOIN GPT_HOTEL H " +
												"ON H.CITY_ID = LN.LOCATION_ID";
	private static final String HOTEL_CATEGORIES = "SELECT DISTINCT CATEGORY FROM GPT_HOTEL";

	@Override
	public List<String> getCitiesNames() {
		return getJdbcTemplate().query(CITIES_NAMES, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int index) throws SQLException {
				return rs.getString("NAME");
			}
		});
	}

	@Override
	public List<String> getHotelCategories() {
		return getJdbcTemplate().query(HOTEL_CATEGORIES, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int index) throws SQLException {
				return rs.getString("CATEGORY");
			}
		});
	}
}
