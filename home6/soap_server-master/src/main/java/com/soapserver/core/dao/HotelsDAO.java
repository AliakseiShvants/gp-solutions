package com.soapserver.core.dao;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface HotelsDAO {

    @Transactional(
            readOnly = true,
            rollbackFor = Throwable.class
    )
    List<String> getCitiesNames();

    @Transactional(
            readOnly = true,
            rollbackFor = Throwable.class
    )
    List<String> getHotelCategories();
}
