package com.soapserver.core.filters.impl;

import com.soapserver.core.dao.HotelsDAO;
import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;

import java.util.List;

public class CheckCategoryFilter implements PreFilter<HotelsRequest> {

    private HotelsDAO hotelsDAO;

    @Override
    public boolean isApplicable(final HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(final HotelsRequest request) throws ServiceException {
        if (request.getHotelCategory().length() > 4) {
            final List<String> categories = hotelsDAO.getHotelCategories();
            if (!categories.contains(request.getHotelCategory().toUpperCase())) {
                throw new ServiceException("Requested category is incorrect");
            }
        }
    }

    public void setHotelsDAO(HotelsDAO hotelsDAO) {
        this.hotelsDAO = hotelsDAO;
    }
}
