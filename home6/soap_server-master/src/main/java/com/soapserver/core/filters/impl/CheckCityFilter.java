package com.soapserver.core.filters.impl;

import com.soapserver.core.dao.HotelsDAO;
import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;

import java.util.List;

public class CheckCityFilter implements PreFilter<HotelsRequest> {

    private HotelsDAO hotelsDAO;

    @Override
    public boolean isApplicable(final HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(final HotelsRequest request) throws ServiceException {
        if (request.getCityName().length() >= 5) {
            final List<String> citiesNames = hotelsDAO.getCitiesNames();
            if (!citiesNames.contains(request.getCityName().toUpperCase())) {
                throw new ServiceException("Requested city name is incorrect");
            }
        }
    }

    public void setHotelsDAO(HotelsDAO hotelsDAO) {
        this.hotelsDAO = hotelsDAO;
    }
}
