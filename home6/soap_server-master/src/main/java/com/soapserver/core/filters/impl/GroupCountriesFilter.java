package com.soapserver.core.filters.impl;

import com.soapserver.core.filters.PostFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.Country;
import com.soapserver.entities.Hotel;
import com.soapserver.entities.HotelsRequest;
import com.soapserver.entities.HotelsResponse;

import java.util.*;

public class GroupCountriesFilter implements PostFilter<HotelsRequest, HotelsResponse> {

    @Override
    public boolean isApplicable(final HotelsRequest request, final HotelsResponse response) {
        return !response.getHotel().isEmpty();
    }

    @Override
    public void postProcess(final HotelsRequest request, final HotelsResponse response) throws ServiceException {
        final List<Hotel> hotelsDeleteList = new ArrayList<>();
        final Map<String, Country> countriesMap = new HashMap<>();

        for (final Hotel hotel : response.getHotel()) {
            final String hotelName = hotel.getHotelRequisites().getTitle();
            if (countriesMap.containsKey(hotelName)) {
                hotelsDeleteList.add(hotel);
            }

            if (hotel.getCountry() == null) {
                continue;
            }

            Country country = countriesMap.get(hotelName);
            if (country == null) {
                countriesMap.put(hotelName, country);
            }
            country = hotel.getCountry();
        }

        response.getHotel().removeAll(hotelsDeleteList);

        final Iterator<Hotel> hotelIterator = response.getHotel().iterator();

        while (hotelIterator.hasNext()) {
            final Hotel hotel = hotelIterator.next();
            Country country = hotel.getCountry();
            if (country == null) {
                continue;
            }

            final Country country1 = countriesMap.get(hotel.getHotelRequisites().getTitle());
            if (country1 != null) {
                country = country1;
            }
        }
    }
}
