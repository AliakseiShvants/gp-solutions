<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://soapserver.com/entities"
                xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
                xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
                exclude-result-prefixes="ent sutil queryutil">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:variable name="images_info" select="queryutil:subQuery('SELECT HI.URL URL, IT.TYPE_NAME TYPE, HI.HOTEL_ID ID FROM GPT_HOTEL_IMAGES HI JOIN GPT_IMAGE_TYPE IT ON HI.TYPE_ID = IT.ID')"/>

        <ent:HotelsResponse>

            <xsl:for-each select="Result/Entry">
                <xsl:variable name="country" select="COUNTRY"/>
                <xsl:variable name="countryId" select="COUNTRY_ID"/>
                <xsl:variable name="city" select="CITY"/>
                <xsl:variable name="cityId" select="CITY_ID"/>
                <xsl:variable name="hotel" select="HOTEL"/>
                <xsl:variable name="code" select="CODE"/>
                <xsl:variable name="phone" select="PHONE"/>
                <xsl:variable name="fax" select="FAX"/>
                <xsl:variable name="email" select="EMAIL"/>
                <xsl:variable name="hotel_url" select="URL"/>
                <xsl:variable name="checkIn" select="CHECK_IN_TIME"/>
                <xsl:variable name="checkOut" select="CHECK_OUT_TIME"/>
                <xsl:variable name="coord" select="COORDINATES"/>

                <xsl:variable name="hotel_id" select="HOTEL_ID"/>
                <xsl:variable name="images" select="$images_info/Entry[ID = $hotel_id]"/>

                <ent:Hotel>
                    <ent:Country>
                        <ent:Title>
                            <xsl:value-of select="$country"/>
                        </ent:Title>
                        <ent:Code>
                            <xsl:value-of select="$countryId"/>
                        </ent:Code>
                    </ent:Country>
                    <ent:City>
                        <ent:Title>
                            <xsl:value-of select="$city"/>
                        </ent:Title>
                        <ent:Code>
                            <xsl:value-of select="$cityId"/>
                        </ent:Code>
                    </ent:City>
                    <ent:HotelRequisites>
                        <ent:Title>
                            <xsl:value-of select="$hotel"/>
                        </ent:Title>
                        <ent:Code>
                            <xsl:value-of select="$code"/>
                        </ent:Code>
                        <ent:Phone>
                            <xsl:value-of select="$phone"/>
                        </ent:Phone>
                        <ent:Fax>
                            <xsl:value-of select="$fax"/>
                        </ent:Fax>
                        <ent:Email>
                            <xsl:value-of select="$email"/>
                        </ent:Email>
                        <ent:Url>
                            <xsl:value-of select="$hotel_url"/>
                        </ent:Url>
                        <ent:CheckIn>
                            <xsl:value-of select="$checkIn"/>
                        </ent:CheckIn>
                        <ent:CheckOut>
                            <xsl:value-of select="$checkOut"/>
                        </ent:CheckOut>
                        <ent:Coordinates>
                            <xsl:value-of select="$coord"/>
                        </ent:Coordinates>
                        <ent:Images>
                            <xsl:for-each select="$images">
                                <xsl:variable name="image_url" select="URL"/>
                                <xsl:variable name="image_type" select="TYPE"/>
                                <ent:Image>
                                    <ent:Url>
                                        <xsl:value-of select="$image_url"/>
                                    </ent:Url>
                                    <ent:Type>
                                        <xsl:value-of select="$image_type"/>
                                    </ent:Type>
                                </ent:Image>
                            </xsl:for-each>
                        </ent:Images>
                    </ent:HotelRequisites>
                </ent:Hotel>
            </xsl:for-each>
        </ent:HotelsResponse>
    </xsl:template>
</xsl:stylesheet>