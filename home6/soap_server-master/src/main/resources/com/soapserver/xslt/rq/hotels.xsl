<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://soapserver.com/entities"
                xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
                xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
                xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
                exclude-result-prefixes="ent requtil">
    <xsl:output method="text"/>

    <xsl:template match="/">
        <xsl:variable name="countryCode" select="//ent:CountryCode"/>
        <xsl:variable name="cityName" select="sutil:upperCase(//ent:CityName)"/>
        <xsl:variable name="lang" select="sutil:upperCase(//ent:Language)"/>
        <xsl:variable name="hotelNamePart" select="sutil:upperCase(//ent:HotelNamePart)"/>
        <xsl:variable name="hotelCategory" select="sutil:upperCase(//ent:HotelCategory)"/>

        <xsl:variable name="langIdQuery">
            <xsl:text>SELECT ID FROM GPT_LANGUAGE WHERE CODE = '</xsl:text>
            <xsl:value-of select="$lang"/>
            <xsl:text>'</xsl:text>
        </xsl:variable>
        <xsl:variable name="langId" select="queryutil:subQuery($langIdQuery)"/>

        <xsl:variable name="hotelsQuery">
            <xsl:text>SELECT HOTEL_ID, LN.NAME COUNTRY, COUNTRY_ID, LN2.NAME CITY, CITY_ID, HN.NAME HOTEL, CODE, PHONE, FAX, </xsl:text>
            <xsl:text>EMAIL, URL, CHECK_IN_TIME, CHECK_OUT_TIME, CONCAT(LATITUDE, ',', LONGTITUDE) COORDINATES </xsl:text>
            <xsl:text>FROM GPT_HOTEL H </xsl:text>
            <xsl:text>JOIN GPT_HOTEL_NAME HN ON H.ID = HN.HOTEL_ID </xsl:text>
            <xsl:text>JOIN GPT_LOCATION_NAME LN ON H.COUNTRY_ID = LN.LOCATION_ID </xsl:text>
            <xsl:text>JOIN GPT_LOCATION_NAME LN2 ON H.CITY_ID = LN2.LOCATION_ID </xsl:text>
            <xsl:text>WHERE HN.LANG_ID = </xsl:text>
            <xsl:value-of select="$langId"/>
            <xsl:text> AND LN.LANG_ID = </xsl:text>
            <xsl:value-of select="$langId"/>
            <xsl:text> AND LN2.LANG_ID = </xsl:text>
            <xsl:value-of select="$langId"/>
            <xsl:text> AND COUNTRY_ID = </xsl:text>
            <xsl:value-of select="$countryCode"/>
        </xsl:variable>

        <xsl:if test="not(number($countryCode))">
            <xsl:text>Country code must be a number!</xsl:text>
            <xsl:message terminate="yes">
                <xsl:value-of select="requtil:overrideResponse()"/>
            </xsl:message>
        </xsl:if>

        <xsl:value-of select="$hotelsQuery"/>

        <xsl:if test="string-length($cityName) &gt; 0">
            <xsl:text> AND LN2.NAME LIKE '%</xsl:text>
            <xsl:value-of select="$cityName"/>
            <xsl:text>%'</xsl:text>
        </xsl:if>

        <xsl:if test="string-length($hotelNamePart) &gt; 0">
            <xsl:text> AND HN.NAME LIKE '%</xsl:text>
            <xsl:value-of select="$hotelNamePart"/>
            <xsl:text>%'</xsl:text>
        </xsl:if>

        <xsl:if test="string-length($hotelCategory) &gt; 0">
            <xsl:text> AND CATEGORY = '</xsl:text>
            <xsl:value-of select="$hotelCategory"/>
            <xsl:text>'</xsl:text>
        </xsl:if>

        <xsl:text>;</xsl:text>
    </xsl:template>
</xsl:stylesheet>