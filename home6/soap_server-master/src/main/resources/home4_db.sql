-- MySQL dump 10.13  Distrib 5.5.23, for Win64 (x86)
--
-- Host: localhost    Database: home4
-- ------------------------------------------------------
-- Server version	5.5.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gpt_hotel`
--

DROP TABLE IF EXISTS `gpt_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(32) DEFAULT NULL,
  `CITY_ID` bigint(20) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `CATEGORY` enum('HOTEL','HOSTEL','APARTMENT') DEFAULT NULL,
  `CHAIN_ID` bigint(20) DEFAULT '1',
  `PHONE` varchar(100) DEFAULT NULL,
  `FAX` varchar(50) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `CHECK_IN_TIME` time DEFAULT NULL,
  `CHECK_OUT_TIME` time DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `ZIP` varchar(50) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `LATITUDE` decimal(10,6) DEFAULT NULL,
  `LONGTITUDE` decimal(10,6) DEFAULT NULL,
  `ACTIVE` bit(1) DEFAULT NULL,
  `DEFAULT_IMAGE` bigint(20) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel`
--

LOCK TABLES `gpt_hotel` WRITE;
/*!40000 ALTER TABLE `gpt_hotel` DISABLE KEYS */;
INSERT INTO `gpt_hotel` VALUES (1,'BYH001',3,1,'HOTEL',1,'+375291011010','+375297007070','https://www.booking.com/pekinhotelminsk','14:00:00','12:00:00','pekin@com','220030','Hotel str. 1-1',53.800000,23.500000,'',5,'2017-01-01 12:00:00'),(2,'BYH002',3,1,'HOTEL',1,'+375291021010','+375297017070','https://www.booking.com/doubletreebyhilton','14:00:00','12:00:00','dtbhilton@com','220004','Hotel str. 2-2',53.700000,23.600000,'',4,'2017-02-01 11:00:00'),(3,'BYH003',3,1,'APARTMENT',2,'+375297021010','+375296017070','https://www.booking.com/MINSKLUX','14:00:00','12:00:00','MINSKLUX@com','220000','K.Marks str. 12/2',53.700000,23.600000,'',4,'2017-02-06 19:00:00'),(4,'BYH004',3,1,'APARTMENT',2,'+375297021010','+375296017070','https://www.booking.com/MINSKPREMIUM','14:00:00','12:00:00','MINSKPREMIUM@com','220000','Engels str. 11',53.700000,23.600000,'',4,'2017-03-06 19:00:00'),(5,'BYH005',3,1,'HOSTEL',2,'+375297021010','+375296017070','https://www.booking.com/HDT','14:00:00','12:00:00','HDTM@com','220014','Tsetkin str. 9',53.700000,23.600000,'',4,'2017-06-06 19:00:00'),(6,'BYH006',3,1,'HOSTEL',2,'+375297021010','+375296017070','https://www.booking.com/YOUR','14:00:00','12:00:00','YOUR@com','220131','R.Luksemburg str. 13',53.700000,23.600000,'',4,'2017-06-06 19:00:00'),(7,'BYH007',4,1,'HOSTEL',2,'+375297021010','+375296017070','https://www.booking.com/H1915','14:00:00','12:00:00','H1915@com','210001','Budennyj str. 1',53.700000,23.600000,'',3,'2017-06-06 19:00:00'),(8,'BYH008',4,1,'HOSTEL',2,'+375297021010','+375296017070','https://www.booking.com/XO','14:00:00','12:00:00','HXO@com','210026','Rykov str. 2',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(9,'BYH009',4,1,'APARTMENT',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Kamenev str. 3',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(10,'BYH010',4,1,'APARTMENT',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Kalinin str. 3',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(11,'BYH011',7,1,'APARTMENT',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Radek str. 4',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(12,'BYH012',7,1,'HOTEL',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Krasin str. 5',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(13,'BYH013',7,1,'HOSTEL',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Bucharin str. 6',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(14,'BYH014',5,2,'APARTMENT',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Chicherin str. 7',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(15,'BYH015',5,2,'APARTMENT',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Trozkij str. 8',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(16,'BYH016',5,2,'HOTEL',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Rakovskij str. 9',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(17,'BYH017',5,2,'HOSTEL',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','D.Bednyj str. 10',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(18,'BYH018',6,2,'APARTMENT',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Litvinov str. 11',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(19,'BYH019',6,2,'HOTEL',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Stalin str. 12',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(20,'BYH020',6,2,'HOSTEL',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Zinovjev str. 14',53.700000,23.600000,'',5,'2017-06-06 19:00:00'),(21,'BYH021',5,2,'HOSTEL',2,'+375297021010','+375296017070','https://www.booking.com/','14:00:00','12:00:00','MAIL@com','210026','Kauno g. 15',53.700000,23.600000,'',5,'2017-06-06 19:00:00');
/*!40000 ALTER TABLE `gpt_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_images`
--

DROP TABLE IF EXISTS `gpt_hotel_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_images` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HOTEL_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` tinyint(3) DEFAULT NULL,
  `URL` varchar(600) DEFAULT NULL,
  `THUMBNAIL_URL` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `HOTEL_ID` (`HOTEL_ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_2` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_image_type` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_images`
--

LOCK TABLES `gpt_hotel_images` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_images` DISABLE KEYS */;
INSERT INTO `gpt_hotel_images` VALUES (1,1,1,'URL','TN_URL'),(2,1,2,'URL','TN_URL'),(3,1,2,'URL','TN_URL'),(4,1,3,'URL','TN_URL'),(5,1,3,'URL','TN_URL'),(6,1,3,'URL','TN_URL'),(7,2,3,'URL','TN_URL'),(8,2,4,'URL','TN_URL'),(9,3,4,'URL','TN_URL'),(10,5,5,'URL','TN_URL'),(11,5,5,'URL','TN_URL'),(12,5,5,'URL','TN_URL'),(13,11,5,'URL','TN_URL'),(14,11,4,'URL','TN_URL'),(15,11,3,'URL','TN_URL'),(16,11,2,'URL','TN_URL'),(17,13,2,'URL','TN_URL'),(18,13,2,'URL','TN_URL'),(19,17,1,'URL','TN_URL'),(20,18,1,'URL','TN_URL'),(21,18,1,'URL','TN_URL'),(22,19,3,'URL','TN_URL'),(23,20,3,'URL','TN_URL'),(24,20,2,'URL','TN_URL'),(25,21,2,'URL','TN_URL'),(26,21,1,'URL','TN_URL'),(27,21,4,'URL','TN_URL'),(28,21,4,'URL','TN_URL'),(29,21,5,'URL','TN_URL'),(30,21,5,'URL','TN_URL'),(31,7,5,'URL','TN_URL'),(32,8,4,'URL','TN_URL'),(33,8,4,'URL','TN_URL');
/*!40000 ALTER TABLE `gpt_hotel_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_name`
--

DROP TABLE IF EXISTS `gpt_hotel_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_name` (
  `HOTEL_ID` bigint(20) DEFAULT NULL,
  `LANG_ID` tinyint(3) DEFAULT NULL,
  `NAME` varchar(500) DEFAULT NULL,
  KEY `HOTEL_ID` (`HOTEL_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_name`
--

LOCK TABLES `gpt_hotel_name` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_name` DISABLE KEYS */;
INSERT INTO `gpt_hotel_name` VALUES (1,1,'PEKIN'),(2,1,'DOUBLETREE BY HILTON'),(3,1,'MINSKLUX'),(4,1,'MINSKPREMIUM'),(5,1,'HOME DLYA TEBYA'),(6,1,'YOUR'),(7,1,'HOSTEL 1915'),(8,1,'HOSTEL X.O.'),(9,1,'APARTMENTS MINI VIKTORYA'),(10,1,'LUOCHOSA'),(11,1,'EUAPARTMENTS'),(12,1,'KRONON PARK OTEL'),(13,1,'HOSTEL OLD BRIDGE'),(14,1,'VILNIUS APARTMENTS'),(15,1,'LVOVO'),(16,1,'IMPERIAL'),(17,1,'GUEST HOUSE IN OLD TOWN'),(18,1,'NEMUNO STREET APARTMENTS'),(19,1,'IBIS KAUNAS CENTRE'),(20,1,'R HOSTEL'),(21,1,'AMBERTON'),(1,2,'…Šˆ'),(2,2,'„€‹’…… •ˆ‹’Ž'),(3,2,'Œˆ‘Š‹žŠ‘'),(4,2,'Œˆ‘Š…Œˆ“Œ'),(5,2,'„ŽŒ „‹Ÿ ’…Ÿ'),(6,2,'’‚Ž‰'),(7,2,'•Ž‘’…‹ 1915'),(8,2,'•Ž‘’…‹ •.Ž.'),(9,2,'Œˆˆ ‚ˆŠ’ŽˆŸ'),(10,2,'‹“—…‘€'),(11,2,'“‚Ž€€’€Œ…’›'),(12,2,'ŠŽŽ €Š Ž’…‹œ'),(13,2,'‘’€›‰ ŒŽ‘’');
/*!40000 ALTER TABLE `gpt_hotel_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_image_type`
--

DROP TABLE IF EXISTS `gpt_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_image_type` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_image_type`
--

LOCK TABLES `gpt_image_type` WRITE;
/*!40000 ALTER TABLE `gpt_image_type` DISABLE KEYS */;
INSERT INTO `gpt_image_type` VALUES (1,'GENERAL'),(2,'ROOM'),(3,'KITCHEN'),(4,'BATH'),(5,'FROM WINDOW');
/*!40000 ALTER TABLE `gpt_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_language`
--

DROP TABLE IF EXISTS `gpt_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_language` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(40) DEFAULT NULL,
  `CODE` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_language`
--

LOCK TABLES `gpt_language` WRITE;
/*!40000 ALTER TABLE `gpt_language` DISABLE KEYS */;
INSERT INTO `gpt_language` VALUES (1,'ENGLISH','EN'),(2,'“‘‘Šˆ‰','RU'),(3,'LIETUVISKAS','LT');
/*!40000 ALTER TABLE `gpt_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location`
--

DROP TABLE IF EXISTS `gpt_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) DEFAULT NULL,
  `TYPE_ID` tinyint(3) DEFAULT NULL,
  `PARENT` bigint(20) DEFAULT NULL,
  `ACTIVE` bit(1) DEFAULT NULL,
  `LATITUDE` decimal(10,6) DEFAULT NULL,
  `LONGTITUDE` decimal(10,6) DEFAULT NULL,
  `TIME_ZONE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_location_ibfk_1` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_location_type` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location`
--

LOCK TABLES `gpt_location` WRITE;
/*!40000 ALTER TABLE `gpt_location` DISABLE KEYS */;
INSERT INTO `gpt_location` VALUES (1,'BY000',2,1,'',53.528300,28.046700,'+3:00 UTC'),(2,'LT000',2,1,'',55.000000,26.000000,'+2:00 UTC, +3:00 UTC'),(3,'BY001',3,2,'',53.900000,27.566670,'+3:00 UTC'),(4,'BY002',3,2,'',55.190400,30.049000,'+3:00 UTC'),(5,'LT001',3,2,'',54.689200,25.279800,'+2:00 UTC'),(6,'LT002',3,2,'',54.898500,23.903600,'+2:00 UTC'),(7,'BY003',3,2,'',53.669300,23.813100,'+3:00 UTC');
/*!40000 ALTER TABLE `gpt_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_name`
--

DROP TABLE IF EXISTS `gpt_location_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_name` (
  `LOCATION_ID` bigint(20) DEFAULT NULL,
  `LANG_ID` tinyint(3) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  KEY `LOCATION_ID` (`LOCATION_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_location_name_ibfk_1` FOREIGN KEY (`LOCATION_ID`) REFERENCES `gpt_location` (`ID`),
  CONSTRAINT `gpt_location_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_name`
--

LOCK TABLES `gpt_location_name` WRITE;
/*!40000 ALTER TABLE `gpt_location_name` DISABLE KEYS */;
INSERT INTO `gpt_location_name` VALUES (1,1,'BELARUS'),(1,2,'…‹€“‘œ'),(1,3,'BALTARUSIJA'),(2,1,'LITHUANIA'),(2,2,'‹ˆ’‚€'),(2,3,'LIETUVA'),(3,1,'MINSK'),(3,2,'Œˆ‘Š'),(3,3,'MINSKAS'),(4,1,'VITEBSK'),(4,2,'‚ˆ’…‘Š'),(4,3,'VITEBSKAS'),(7,1,'HRODNA'),(7,2,'ƒŽ„Ž'),(7,3,'GARDINAS'),(5,1,'VILNIUS'),(5,2,'‚ˆ‹œž‘'),(5,3,'VILNIUS'),(6,1,'KAUNAS'),(6,2,'Š€“€‘'),(6,3,'KAUNAS');
/*!40000 ALTER TABLE `gpt_location_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_type`
--

DROP TABLE IF EXISTS `gpt_location_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_type` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_type`
--

LOCK TABLES `gpt_location_type` WRITE;
/*!40000 ALTER TABLE `gpt_location_type` DISABLE KEYS */;
INSERT INTO `gpt_location_type` VALUES (1,'root'),(2,'country'),(3,'city');
/*!40000 ALTER TABLE `gpt_location_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-01 11:40:59
